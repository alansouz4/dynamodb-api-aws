package br.com.vx.conciliacao.repositories;

import br.com.vx.conciliacao.models.Conciliacao;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@EnableScan
public interface ConciliacaoRepository {

    Conciliacao salvar(Conciliacao conciliacao);
    Conciliacao obterConcilicaoPorId(String concilicaoId);
    Iterable<Conciliacao> obterDivergentes(LocalDate dtCriacaoConciliacao, String ativo);
    String deletar(String conciliacaoId);
    Conciliacao atualizar(String conciliacaoId, Conciliacao conciliacao);

}
