package br.com.vx.conciliacao.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Conciliado não existe")
public class ConciliadoNotFoundException extends RuntimeException {

}
