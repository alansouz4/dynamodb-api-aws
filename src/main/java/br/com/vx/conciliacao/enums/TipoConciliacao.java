package br.com.vx.conciliacao.enums;

public enum TipoConciliacao {

    BATIDO,
    DIVERGENTE
}
