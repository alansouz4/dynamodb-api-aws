package br.com.vx.conciliacao.services;

import br.com.vx.conciliacao.models.Conciliacao;
import br.com.vx.conciliacao.repositories.ConciliacaoRepository;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBSaveExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ExpectedAttributeValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class ConciliacaoService implements ConciliacaoRepository{

    @Autowired
    private DynamoDBMapper dynamoDBMapper;

    @Override
    public Conciliacao salvar(Conciliacao conciliacao) {
        dynamoDBMapper.save(conciliacao);
        return conciliacao;
    }

    @Override
    public Conciliacao obterConcilicaoPorId(String concilicaoId) {
        return dynamoDBMapper.load(Conciliacao.class, concilicaoId);
    }

    @Override
    public Iterable<Conciliacao> obterDivergentes(LocalDate dtCriacaoConciliacao, String ativo) {
        Iterable<Conciliacao> conc = dynamoDBMapper.batchLoad(dtCriacaoConciliacao, ativo);
        return conc;
    }

    @Override
    public String deletar(String conciliacaoId) {
        Conciliacao conc = dynamoDBMapper.load(Conciliacao.class, conciliacaoId);
        dynamoDBMapper.delete(conc);
        return "Conciliacao deletada!";
    }

    @Override
    public Conciliacao atualizar(String conciliacaoId, Conciliacao conciliacao) {
        dynamoDBMapper.save(conciliacao,
                new DynamoDBSaveExpression()
        .withExpectedEntry("conciliacaoId",
                new ExpectedAttributeValue(
                         new AttributeValue().withS(conciliacaoId)
                )));
        return conciliacao;
    }
}
