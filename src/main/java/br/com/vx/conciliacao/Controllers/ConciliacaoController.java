package br.com.vx.conciliacao.Controllers;

import br.com.vx.conciliacao.enums.TipoConciliacao;
import br.com.vx.conciliacao.models.Conciliacao;
import br.com.vx.conciliacao.services.ConciliacaoService;
import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;

@RestController
@RequestMapping("/conciliado")
public class ConciliacaoController {

    @Autowired
    private ConciliacaoService service;

    @RequestMapping(produces = {"application/json"}, method = RequestMethod.POST)
    public ResponseEntity salvarConcialiacao(@RequestBody Conciliacao conciliacao) {
        try {
            Conciliacao response = service.salvar(conciliacao);
            return ResponseEntity.status(HttpStatus.CREATED).body(response);
        } catch (AmazonServiceException e) {
            throw new ResponseStatusException(HttpStatus.valueOf(e.getStatusCode()), e.getMessage(), e);
        } catch (AmazonClientException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        }
    }

//    @PostMapping
//    @ResponseStatus(HttpStatus.CREATED)
//    public Conciliacao salvarConciliacao(@RequestBody Conciliacao conciliacao) {
//        return service.salvar(conciliacao);
//    }


    @RequestMapping(value = "/{id}", produces = {"application/json"}, method = RequestMethod.GET)
    public ResponseEntity obterDivergentePorId(@PathVariable String conciliacaoId) {
        try {
            Conciliacao response = service.obterConcilicaoPorId(conciliacaoId);
            return ResponseEntity.status(HttpStatus.OK).body(response);
        } catch (AmazonServiceException e) {
            throw new ResponseStatusException(HttpStatus.valueOf(e.getStatusCode()), e.getMessage(), e);
        } catch (AmazonClientException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        }
    }

//    @GetMapping("/{id}")
//    public Conciliacao obterDivergentePorId(@PathVariable("id") String conciliacaoId){
//        return service.obterConcilicaoPorId(conciliacaoId);
//    }

    @RequestMapping(value = "/{tipoDeConciliacao}", produces = {"application/json"}, method = RequestMethod.GET)
    public Iterable<Conciliacao> obterDivergentes(
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
            @RequestParam(name = "tipoDeConciliacao", required = true) LocalDate dtCriacaoConciliacao,
            @RequestParam(name = "ativo", required = true) String ativo){
        if(dtCriacaoConciliacao == null){
            Iterable<Conciliacao> conciliacao = service.obterDivergentes(dtCriacaoConciliacao, ativo);
            return conciliacao;
        }
        Iterable<Conciliacao> conciliacao = service.obterDivergentes(dtCriacaoConciliacao, ativo);
        return conciliacao;
    }

    @RequestMapping(value = "/{id}", produces = {"application/json"}, method = RequestMethod.DELETE)
    public ResponseEntity deleteUser(@PathVariable String conciliacaoId) {
        try {
            service.deletar(conciliacaoId);
            return ResponseEntity.status(HttpStatus.OK).body(null);
        } catch (AmazonServiceException e) {
            throw new ResponseStatusException(HttpStatus.valueOf(e.getStatusCode()), e.getMessage(), e);
        } catch (AmazonClientException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        }
    }


//    @DeleteMapping("/{id}")
//    public String deletar(@PathVariable("id") String conciliacaoId){
//        return service.deletar(conciliacaoId);
//    }

    @RequestMapping(value = "/id", produces = {"application/json"}, method = RequestMethod.PUT)
    public ResponseEntity atualizarConciliacao(@PathVariable("id") String conciliacaoId, @RequestBody Conciliacao conciliacao) {
        try {
            Conciliacao response = service.atualizar(conciliacaoId,conciliacao);
            return ResponseEntity.status(HttpStatus.OK).body(response);
        } catch (AmazonServiceException e) {
            throw new ResponseStatusException(HttpStatus.valueOf(e.getStatusCode()), e.getMessage(), e);
        } catch (AmazonClientException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        }
    }

//    @PutMapping("/{id}")
//    public String Atualizar(@PathVariable("id") String conciliacaoId, @RequestBody Conciliacao conciliacao){
//        return service.atualizar(conciliacaoId, conciliacao);
//    }
}
