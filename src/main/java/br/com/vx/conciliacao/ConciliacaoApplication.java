package br.com.vx.conciliacao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConciliacaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConciliacaoApplication.class, args);
	}

}
